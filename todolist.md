# Idée générale

Récupérer des informations et les mettre sous forme de fichiers facilement intégrables.


# Assemblée nationale

* Récupération de tous les députés
    * via nosdeputes.fr

* Récupération de tous les Compte-rendus intégraux
    * Par législature

* Récupération de toutes les photos des députés (via l'ID ?)
    * Passer par nosdeputes.fr ?


# Sénat

* Récupération de toutes les photos des sénateurs (via l'ID ?)
    * Passer par nossenateurs.fr ?


# LQdN

* Récupérer les prises de positions depuis memopol


