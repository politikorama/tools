# encoding: utf-8
"""
Le but de ce module est de transformer la liste de tous les députés au format CSV en un
fichier facilement importable dans politikorama.

L'idée finale étant de mettre à jour régulièrement cette liste.
"""

from argparse import ArgumentParser
from datetime import datetime
import json
import os
import shutil
import sys
from time import sleep
from zipfile import ZipFile

from bs4 import BeautifulSoup
from slugify import slugify
import requests


TEMP_FOLDER = "tmp/french_assembly"
DATA_FOLDER = "../datas/french_assembly"


def representatives_download():
    """
    Récupère la liste à jour depuis nosdeputes.fr
    """
    os.makedirs(TEMP_FOLDER, exist_ok=True)
    url = "https://data.assemblee-nationale.fr/static/openData/repository/16/amo/tous_acteurs_mandats_organes_xi_legislature/AMO30_tous_acteurs_tous_mandats_tous_organes_historique.json.zip"
    # ~ date = datetime.strftime(datetime.today(), "%Y%m%d")
    filename = f"french_assembly.json.zip"
    filepath = os.path.join(TEMP_FOLDER, filename)
    with requests.get(url, stream=True) as response:
        response.raise_for_status()
        print("Téléchargement de la liste des députés ", end="")
        sys.stdout.flush()
        with open(filepath, "wb") as handler:
            for chunk in response.iter_content(chunk_size=8192):
                print(".", end="")
                sys.stdout.flush()
                handler.write(chunk)
        print("")


def representatives_unzip():
    """
    Décompresse la liste
    """
    os.makedirs(TEMP_FOLDER, exist_ok=True)
    filename = f"french_assembly.json.zip"
    filepath = os.path.join(TEMP_FOLDER, filename)
    liste_zip = ZipFile(filepath)
    liste_zip.extractall(TEMP_FOLDER)


def representatives_parse():
    """
    Utilise la liste des fichiers des députés et en extrait plusieurs fichiers :
    - La liste des députés
    """
    os.makedirs(DATA_FOLDER, exist_ok=True)
    deputes = []
    path = os.path.join(TEMP_FOLDER, "json/acteur")
    print("Extraction de la liste des députés ", end="")
    for filename in sorted(os.listdir(path)):
        print(".", end="")
        sys.stdout.flush()
        filepath = os.path.join(path, filename)
        with open(filepath, encoding="utf-8") as handler:
            datas = json.load(handler)
            datas = datas["acteur"]
            depute = {
                "firstname": "",
                "lastname": "",
                "name": "",
                "picture": "",
                "job": "",
                "birthdate": "",
                "deathdate": "",
                "country": "",
                "mandates": [
                ],
            }
            # Filename is "PA[identifier].json"
            identifier = filename[2:-5]
            gender = "M" if datas["etatCivil"]["ident"]["civ"]=="M." else "F"
            # On récupère les données d'identité de la personne
            depute["firstname"] = datas["etatCivil"]["ident"]["prenom"]
            depute["lastname"] = datas["etatCivil"]["ident"]["nom"]
            depute["name"] = depute["firstname"] + " " + depute["lastname"]
            # On récupère d'autres données intéressantes
            if isinstance(datas["profession"]["libelleCourant"], str):
                depute["job"] = datas["profession"]["libelleCourant"]
            if isinstance(datas["etatCivil"]["infoNaissance"]["dateNais"], str):
                depute["birthdate"] = datas["etatCivil"]["infoNaissance"]["dateNais"]
            if isinstance(datas["etatCivil"]["dateDeces"], str):
                depute["deathdate"] = datas["etatCivil"]["dateDeces"]
            depute["country"] = "France" # on force
            # On récupère les différents mandats de député
            for mandate in datas["mandats"]["mandat"]:
                try:
                    if mandate["typeOrgane"] == "ASSEMBLEE":
                        depute["mandates"].append(
                            {
                                "entity": "Assemblée nationale",
                                "role": "Député" if gender=="M" else "Députée",
                                "start": mandate["mandature"]["datePriseFonction"],
                                "end": mandate["dateFin"],
                            }
                        )
                        legislature = mandate["legislature"]
                except TypeError:
                    # Un seul mandat
                    mandate = datas["mandats"]["mandat"]
                    if mandate["typeOrgane"] == "ASSEMBLEE":
                        depute["mandates"].append(
                            {
                                "entity": "Assemblée nationale",
                                "role": "Député" if gender=="M" else "Députée",
                                "start": mandate["mandature"]["datePriseFonction"],
                                "end": mandate["dateFin"],
                            }
                        )
                        legislature = mandate["legislature"]
            if len(depute["mandates"]) == 0:
                continue
            depute["picture"] = f"https://www.assemblee-nationale.fr/{legislature}/tribun/photos/{identifier}.jpg"
            deputes.append(depute)
    print("")

    print("Sauvegarde de la liste des députés")
    filename = f"french_assembly.json"
    filepath = os.path.join(DATA_FOLDER, filename)
    with open(filepath, "w", encoding="utf-8") as handler:
        json.dump(deputes, handler, indent=2)


def representatives_pictures():
    folder = os.path.join(DATA_FOLDER, "pictures")
    filename = "french_assembly.json"
    filepath = os.path.join(DATA_FOLDER, filename)
    os.makedirs(folder, exist_ok=True)
    with open(filepath) as handler:
        deputes = json.load(handler)
    print("Récupération des photos des députés ", end="")
    for depute in deputes:
        url = depute["picture"]
        slug = slugify(depute["name"])
        print(".", end="")
        sys.stdout.flush()
        if os.path.exists(f"{folder}/{slug}.jpg"):
            continue
        # Do not DDOS the french assembly website
        sleep(1)
        with requests.get(url, stream=True) as response:
            with open(f"{folder}/{slug}.jpg", "wb") as handler:
                for chunk in response.iter_content(chunk_size=8192):
                    handler.write(chunk)
    print("")


def representatives_clean():
    filename = f"french_assembly.json.zip"
    filepath = os.path.join(TEMP_FOLDER, filename)
    if os.path.exists(filepath):
        os.remove(filepath)
    filepath = os.path.join(TEMP_FOLDER, "json")
    shutil.rmtree(filepath, ignore_errors=True)


def stances_download():
    url = "https://www.assemblee-nationale.fr/11/cri/html/%s%s.asp"
    for year in range(1999, 2002):
        path = os.path.join(TEMP_FOLDER, "cri", str(year))
        os.makedirs(path, exist_ok=True)
        print(f"{year} ")
        for i in range(1, 300):
            print(".", end="")
            sys.stdout.flush()
            next_url = url % (year, f"{i:04}")
            filename = "%s_%s.html" % (year, f"{i:04}")
            filepath = os.path.join(path, filename)
            if os.path.exists(filepath):
                continue
            sleep(1)
            with requests.get(next_url, stream=True) as response:
                if response.status_code == 200:
                    with open(filepath, "wb") as handler:
                        for chunk in response.iter_content(chunk_size=8192):
                            handler.write(chunk)
        print("")


def stances_parse():
    stances = []
    print("Extraction de la liste des prises de position ")
    for year in range(1999, 2002)[:1]:
        print(f"{year} ", end="")
        path = os.path.join(TEMP_FOLDER, "cri", str(year))
        for filename in os.listdir(path)[13:14]:
            print(".", end="")
            sys.stdout.flush()
            filepath = os.path.join(path, filename)
            with open(filepath, encoding="latin-1") as handler:
                soup = BeautifulSoup(handler.read(), features="html.parser")
                parag_square = soup.find_all("p")
                for paragraphes in parag_square[:89]:
                    for paragraph in paragraphes.find_all("p"):
                        print(paragraph.text)
                        print("=" * 42)
                # ~ stance = {
                    # ~ "matter": "",
                    # ~ "date": "",
                    # ~ "representative": "",
                    # ~ "stance": "",
                # ~ }


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "--representatives",
        choices=["download", "unzip", "parse", "pictures", "clean"],
        help="to update list of representatives",
    )
    parser.add_argument(
        "--stances",
        choices=["download", "unzip", "parse", "pictures"],
        help="to update list of stances",
    )
    args = parser.parse_args()

    # What to do with representatives
    if args.representatives == "download":
        representatives_download()
    elif args.representatives == "unzip":
        representatives_unzip()
    elif args.representatives == "parse":
        representatives_parse()
    elif args.representatives == "pictures":
        representatives_pictures()
    elif args.representatives == "clean":
        representatives_clean()

    if args.stances == "download":
        stances_download()
    elif args.stances == "parse":
        stances_parse()
